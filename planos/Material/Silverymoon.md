# Tipo de nota: mapa

## pertence ao plano:  [[Material]]

Silverymoon foi muitas vezes apelidado de "a Jóia do Norte",  por se assemelhar e emular a cidade élfica (anteriormente) perdida de Myth Drannor em muitos aspectos. Era um dos poucos lugares civilizados no meio da selva áspera e indomável que era o norte de Faerûn . Tinha uma rica vida cultural e era conhecida como ponto de encontro de todas as raças moralmente inclinadas ao bem . Mesmo os ocasionais drows com boas intenções poderiam eventualmente encontrar hospitalidade na cidade