# Tipo de nota: plano de existencia

O plano Prime Material (também visto como Prime Material Plane ) é o plano em que Toril e o cosmos circundante existem. A frase "Plano Material Prime", ou simplesmente "Prime", é geralmente usada por seres de outros planos no multiverso (notavelmente Sigil ), e "primer" é usado pelos mesmos seres para se referir aos habitantes do Plano Material

	Quer conhecer o Prime, visite-o. Os mundos ilimitados desse plano têm uma variedade infinita, assim como os planos , mas não posso encapsulá-los como fiz aqui. Basta dizer que eles são o nascimento dos Planos Exteriores , os filhos do Interior , e possuem um potencial ilimitado dentro de seus limites
	— O espírito Asonje