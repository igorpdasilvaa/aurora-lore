# Tipo de nota: Aventura

### Grupo realizador:  [[herois de Ampheil]]

Nossos aventureiros, inicialmente formados por Búrzo, Cari, Rubeo, Nyssia e Aiz, se encontraram pela primeira vez em [[Waterdeep]], embora alguns deles já se conhecessem, todos eles estavam em busca de se tornarem aventureiros para concluir seus próprios objetivos
Com isso receberam uma missão de [[Durin]] para levar uma caixa com armamentos e materiais para um outro ferreiro chamado Ghelryn Foehammer localizado em [[triboar]], a missão era perigosa pois na long road havia vários ataques a caravanas e eles eram constantes devido a uma tribo de Orcs que havia nas redondesas.
Na volta pararam para descansar em [[Ampheil]] que sofreu o maior ataque de Orc já visto na região, e foi nesse momento em que cada um dos membros desse recem formado grupo de aventureiros arriscaram a própria vida para proteger a cidade, durante a ardua batalha quando todos já estavam sem esperanças receberam uma ajuda final de um jovem dragão de cobre chamado [[Bazire]] que chegou para garatir a vitória dos nossos aventureiros.
Com isso a noticia se espalhou e em pouco tempo todas as cidades ao redor ficaram sabendo da bravura dos nossos aventureiros, e assim ficaram conhecidos como os [[herois de Ampheil]]