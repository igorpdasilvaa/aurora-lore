# Tipo de nota: Aventura

### Grupo realizador:  [[herois de Ampheil]]

Após o termino do torneio em waterdeep que acabou sem vencedores graças a [[Kimbatul]], nossos heróis procuram a maga [[Laeral Silverhand]] em busca de ajuda para saber uma forma de quebrar a maldição, durante o caminho para a torre do cajado negro Nyssia e Aiz descobrem que o portão negro de baldurs gate caiu e que foi apenas um unico soldado que conseguiu fazer isso, sem fazer alarde, O grupo como um todo vai para a torre e consegue as informações necessárias sobre como curar a maldição que está presente nos druidas e quase que de imediato partem rumo a baldur's gate, eles vão navegando a bordo do navio do capitão [[Ander]] que tinha como seu principal passageiro um comerciante extremamente rico chamado [[Bartolomeu]]
Durante a viagem foram abordados por alguns Merrenoloths que vieram buscar as vidas dos druidas, Ander tentou ajudar com algumas moedas até então diferentes que ele possuia, mas não funcionou, durante uma tempestade horrivel nossos aventureiros foram obrigados a enfrentar aquelas terriveis criaturas.
Foi uma batalha dificil, porém os grandes herois de ampheil venceram. 
Nossos herois chegam em [[Baldur's gate]] e em uma conversa com o atual lider de baldur's gate [[Ulder ravenguard]], descobrem que o ataque foi feito por um dos lacaios de quem seria o futuro noivo da Aiz Walleinstein.
