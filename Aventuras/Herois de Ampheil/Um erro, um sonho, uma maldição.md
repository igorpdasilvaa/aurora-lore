# Tipo de nota: Aventura

### Grupo realizador:  [[herois de Ampheil]]

Durante o descanso imediatamente após a batalha final de [[Aventura inesperada]] nossos herois são todos capturados por uma magia de uma grande mago chamado [[Geth]], aparentemente essa magia transportou todos os nosssos herois para algo parecido com um plano dos sonhos, onde o mago tinha o total controle.
No sonho ele diz ter se engano que na verdade era pra ter entrado em contato com outro grupo, os [[Cavaleiros errantes]], mas mesmo após falar sobre o seu erro ele manteve a magia e continuou interagindo com o grupo. Durante essa conversa é dito que [[Cari Naghere Nui]] e [[Rubeo Naghere Nui]] estão infectados com a agua do rio estige, rio esse utilizado para navegação das criaturas dos planos inferiores, Nossos druidas agora sabem que possuem mais ou menos mais 6 meses de vida e que apenas um clerigo poderozo poderia ajuda-los  
