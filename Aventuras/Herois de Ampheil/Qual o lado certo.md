# Tipo de nota: Aventura

### Grupo realizador:  [[herois de Ampheil]]

Os herois de ampheil começam a recobrar a conciencia e enquanto ainda zonzos começam a sentir cheiro de enxofre e um calor acima do normal, ao levantarem veem [[Karash]] aguardando que eles se recupressarem, e então o meio-orc clérigo diz: Bem vindos a [[Avernus]].

Nossos aventureiros são recepcionados pelos [[Cavaleiros errantes]] que os informam que agora [[Baldur's gate]] está sofrendo um grande ataque, onde [[Bahamut]] desceu dos ceus junto de diversos dragões cromaticos tendo como destino avernus, porém eles foram interceptados pelos mais grandiosos herois existentes na costa da espada, [[Ulder ravenguard]], [[Velk Van Heltz di Clore III]] [[Damakos]] tomaram a frente para interceptá-lo

Após receberm as informações nossos herois recebem também uma nova missão, [[Aldebaran]] diz que eles ainda são fracos para sairem em busca de resolver o problema da poluição do rio styg e que deveriam ir até uma torre de uma maga chamada [[Lerissa]] pois a mesma estava em uma posição extremamente estrategica porém se manteve neutra durante boa parte do tempo, a missão é: traga-a como uma nova aliada ou matem ela.

Nossos herois então saem em busca de cumprir a missão, ao chegar na torre decidem por entrar por vias normais para tentar primeiramente a diplomacia, [[Manigold]] consegue adentrar após dizer que seu pai, [[Sisifo]] o regicida, possuia interessses ali, o restante do grupo também entra e começam a conversar com a maga.

Após um tempo conversando os aventureiros percebem que não vão conseguir traze-la como aliada pois um dos objetivos dela é destruir os cavaleiros errantes por um dia terem derrotado o seu pai em batalha, e então uma batalha se inicia, em determinado momento o raio de disintegração é lançado contra Cari que foi salvo da morte certa pelo seu irmão, a batalha se estende mais com uma estreita vitória para os herois de ampheil que ocorreu após Lerissa se evadir utilizando magia para se teletransportar.

Com isso a calmaria voltou a pairar sob a torre, após um tempo buscando por objetos uteis nossos aventureiros encontram no ultimo andar da torre algo que lhes gelou a alma, um sarcofago, assim como aquele que já é conhecido pelo grupo que havia no templo de kelemvor em baldur's gate, ao remover a tampa eles veem que lerissa está lá dentro, conciente e com todos os seus machucados restaurados, ela questiona se devem continuar a batalha ou se os aventureiros preferem apeans sair da torre e ir embora. 

Cansados e cheios de machucados nossos herois fazem a escolha mais sabia naquele momento, recuar, Lerrisa então os questiona se estão do lado correto para que o mundo não seja destruido, os acompanham até a saida de sua torre e permite que eles saiam com todos os objetos que tinha adquirido enquanto loteavam, a missão não foi um sucesso, porém os aventureiros agora possuem uma nova pergunta na cabeça: estamos do lado certo ? 