# Tipo de nota: Aventura

### Grupo realizador: [[Cavaleiros errantes]]

A aventura começou quando o grupo recebeu uma carta do rei de [[Silverymoon]], [[Velk Van Heltz di Clore III]], a carta pedia por ajuda para investigar o assasinado do filho do rei. prontamente o grupo partiu para esse belo reino situado entre montanhas. 
Ao chegar o rei explicou onde foi encontrado o corpo de seu filho e também onde seria os aposentos dos aventureiros. todos ficariam no proprio castelo do rei, e assim começaram as buscas, todas as pistas indicavam que seu melhor amigo e também maior rival teria ordenado o assasinato, seu nome era [[Damakos]], um tiefling que conseguiu se tornar o arqueduque de [[Stygia]].
Após mais investigações e algumas batalhas o grupo então descobre que o mão direita do rei, [[Vondal]] não era de fato o mão direita e sim um doopleganger, e mais, que o rei não era o rei e na verdade era [[Graz'zt]] o principe do abismo. E após uma ardua batalha eles conseguiram derrotar o impostor e por fim salvar o verdadeiro rei, que no final revela nunca ter tido um filho.
Altair arrependido por ter causado problemas a [[Damakos]] oferece a sua propria cabeça como pedido de desculpas pelos problemas que causaram, mas damakos tinha planos maiores... Nesse dia o tiefling mais poderoso dos nove infernos conseguiu adiquirir para o seu exercito 5 grandes aventureiros.